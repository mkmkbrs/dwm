# DWM

![img](https://gitlab.com/mkmkbrs/dwm/-/raw/master/screenshot.png)

A personal build of the [Suckless Dynamic Window Manager](https://dwm.suckless.org/) (v. 6.5) minus the personal stuff.

## Dependencies

### Programs

- [dmenu](https://tools.suckless.org/dmenu/) - Dynamic menu for X

### Fonts

- [noto-sans](https://github.com/googlefonts/noto-fonts)

## Preconfiguration

Set your terminal and browser under the **commands section** of **config.h**:

    /* commands */
    static const char *termcmd[] = { "your_term", NULL };
    static const char *browser[] = { "your_browser", NULL };

## Installation

In the dwm directory run:

    make && sudo make intsall

Ignore the warning about unused combotag function.

    dwm.c:297:1: warning: ‘combotag’ defined but not used [-Wunused-function]
     combotag(const Arg *arg) {
     ^~~~~~~~

## Troubleshooting

### Ubuntu/Debian

#### Missing Xinerama.h header

Install libxinerama-dev:
    
    sudo apt install libxinerama-dev

### OpenBSD

#### &rsquo;ft2build.h&rsquo; file not found

Before compiling *any* Suckless utilities on OpenBSD, uncomment the following
line in the **config.mk** file:
    
    FREETYPEINC = ${X11INC}/freetype2

## Custom keybindings

By default the Mod key is set to <kbd>Win</kbd>.

### Launching programms

- <kbd>Mod</kbd>+<kbd>Shift</kbd>+<kbd>Enter</kbd> - Launch the terminal
- <kbd>Mod</kbd>+<kbd>x</kbd> - Launch the browser

### Navigation

- <kbd>Mod</kbd>+<kbd>w</kbd> - Close the window
- <kbd>Mod</kbd>+<kbd>TagNum</kbd> - View multiple tags (Same as Mod+Ctrl+TagNum)
- <kbd>Mod</kbd>+<kbd>s</kbd> - Tag selected window to all tags (Same as Mod+Shift+0)
- <kbd>Mod</kbd>+<kbd>n</kbd> - Cycle through monitors (Same as Mod+,/Mod+.)

### DWM Controls

- <kbd>Mod</kbd>+<kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>q</kbd> - Restart dwm

## Installed patches

- [combo](https://dwm.suckless.org/patches/combo)
- [noborder](https://dwm.suckless.org/patches/noborder)
- [hide_vacant_tags](https://dwm.suckless.org/patches/hide_vacant_tags)
- [restartsig](https://dwm.suckless.org/patches/restartsig)
- [sticky](https://dwm.suckless.org/patches/sticky)
- [stickyindicator](https://dwm.suckless.org/patches/stickyindicator)
- [accessnthmonitor (optional)](https://dwm.suckless.org/patches/accessnthmonitor/)
