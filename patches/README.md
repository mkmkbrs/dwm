# How to patch

If you wish **to apply additional patches**, place them here and run `patch -p1 < patches/patch_name.diff` from the `dwm` folder.

